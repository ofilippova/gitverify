patterns:
    $DateTime = @duckling.time
    
require: slotfilling/slotFilling.sc
    module = sys.zb-common
    
theme: /

    state: /Start
        q!: $regex</start>
        a: Начнём. обновила мастер!!!
    
    state: GOOD
        intent: /хорошо
        a: 1
                    
    state: BAD
        intent: /плохо
        a: 2
        
    state: Hi
        intent!: /привет
        a: Привет! как дела?
        go!: /Hi/wassup
            
        state: wassup
            
            state: хорошо
                intent: /хорошо
                a: рад за тебя
                    
            state: плохо
                intent: /плохо
                a: не грусти, все будет нормально
                
    state:
        event!: noMatch
        a: Я не понял. Вы сказали: {{$request.query}}

